
const BASE_URL = "https://63dd2128367aa5a7a4099979.mockapi.io/capstone";
var DS = [];
var SP = [];
var gioHang = [];
let fetchQLSPList = () => {
    axios({
        url: BASE_URL,
        method: "GET",
    })

        .then((res) => {
            renderDanhSachSP(res.data);

            DS.push(...res.data);
        })

        .catch((err) => {
            console.log(err)
        });
}

fetchQLSPList();


document.getElementById("loaiSanPham").addEventListener('change', function () {
    var select = this.value;
    SP = DS.filter(function (product) {
        return product.type === select;
    });
    if (SP.length === 0) {
        return renderDanhSachSP(DS);
    }

    renderDanhSachSP(SP);
});

let themSP = (id) => {
    let index = DS.findIndex(function (item) {
        return item.id == id;
    });
    console.log(index);
    let vitri = gioHang.findIndex(function (item) {
        return item.product.id == id;
    });
    console.log(vitri);
    if (vitri == -1) {
        var cartitem = {
            product: DS[index],
            quantity: 1,
        }
        gioHang.push(cartitem);

    } else {
        gioHang[vitri].quantity += 1;
    }
    console.log(gioHang);

    console.log(tongSLSP(gioHang));
    rendertgiohang(gioHang);
    setgiohang();
}

let tongSLSP = (Arr) => {
    console.log(Arr);
    let tong = 0;
    for (let i = 0; i < Arr.length; i++) {
        tong = tong + Arr[i].quantity;
    }
    console.log(tong);
    return tong;

}
let rendertgiohang = (qlgh) => {
    var contenHTML = qlgh.map(function (item) {
        return ` 
        <div class="cartitem1">
                    <img src="${item.product.img}" alt="">
                    <div> ${item.product.name}</div>
                    <div>
                        <button onClick="giamSL(${item.product.id})" class="tang"> <i class="fa fa-angle-left"></i></button>
                        <span>${item.quantity}</span>
                        <button onClick="tangSL(${item.product.id})" class="giam"><i class="fa fa-angle-right"></i></button>
                    </div>
                    <div>${item.quantity * item.product.price}</div>
                    <buttom onClick="XoaSP(${item.product.id})">Xóa</buttom>
                </div>
        `
    }).join('');
    document.getElementById('rendergiohang').innerHTML = contenHTML;
    document.getElementById('tongTTT').innerHTML = tongTTT(qlgh);
    document.getElementById('tongSLSP').innerHTML = tongSLSP(gioHang);
}
let tongTTT = (giohang) => {
    let tongSL = 0;
    for (let i = 0; i < giohang.length; i++) {
        tongSL = tongSL + (giohang[i].quantity * giohang[i].product.price);
    }

    console.log(tongSL);
    return tongSL;

}
let tangSL = (id) => {
    let index = DS.findIndex(function (item) {
        return item.id == id;
    });
    console.log(index);
    let vitri = gioHang.findIndex(function (item) {
        return item.product.id == id;
    });
    console.log(vitri);
    if (vitri == -1) {
        var cartitem = {
            product: DS[index],
            quantity: 1,
        }
        gioHang.push(cartitem);

    } else {
        gioHang[vitri].quantity += 1;
    }
    console.log(gioHang);
    document.getElementById('tongSLSP').innerHTML = tongSLSP(gioHang);
    console.log(tongSLSP(gioHang));
    rendertgiohang(gioHang);
    setgiohang();
}
let giamSL = (id) => {

    let vitri = gioHang.findIndex(function (item) {
        return item.product.id == id;
    });
    console.log(vitri);
    gioHang[vitri].quantity -= 1;
    if (gioHang[vitri].quantity == 0) {
        gioHang.splice(vitri, 1)
    }
    console.log(gioHang);
    document.getElementById('tongSLSP').innerHTML = tongSLSP(gioHang);
    rendertgiohang(gioHang);
    setgiohang();
}
let XoaSP = (id) => {
    let vitri = gioHang.findIndex(function (item) {
        return item.product.id == id;
    });
    gioHang.splice(vitri, 1)
    console.log(gioHang);
    document.getElementById('tongSLSP').innerHTML = tongSLSP(gioHang);
    rendertgiohang(gioHang);
    setgiohang();
}

let clearSP = () => {
    gioHang = [];
    console.log(gioHang);
    document.getElementById('tongSLSP').innerHTML = tongSLSP(gioHang);
    rendertgiohang(gioHang);
    setgiohang();
}
let setgiohang = () => {
    var dataJson = JSON.stringify(gioHang);
    localStorage.setItem('LOCAL_CART', dataJson);
}
let laygiohang = () => {
    var dataJson = localStorage.getItem('LOCAL_CART');
    if (dataJson != null) {
        gioHang = JSON.parse(dataJson);
        console.log(gioHang);
        rendertgiohang(gioHang);

    }
}
laygiohang();
