let renderDanhSachSP = (qlspArr) => {
    var contentHTML = "";
    qlspArr.forEach((DS) => {
        var contentTr = `
        <div class="item" data-key="1">
        <div class="img">
            <img src="${DS.img}" alt="">
        </div>
        <div class="content">
            <div class="title">${DS.name}</div>
            <div class="desc">
                ${DS.desc}
            </div>
            <div class="price">${DS.price}</div>
            
            
            <button onclick="themSP(${DS.id})" class="add">Add</button>
            
        </div>
    </div>
        `;

        contentHTML += contentTr;
    });

    document.getElementById('items-container').innerHTML = contentHTML;
}
let shop = () => {
    document.getElementById('cart-show').style.display = 'block';
}

document.getElementById('close-btn').addEventListener('click', function () {
    document.getElementById('cart-show').style.display = 'none';
});


